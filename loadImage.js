const Jimp = require('jimp');

exports.loadImage = img => {
    let data = [];
    let H, W;
    Jimp
    .read(img)
    .then(image => {
        W = image.bitmap.width;
        H = image.bitmap.height;
        for (let x = 0; x < W; x++) {
            let row = [];
            for (let y = 0; y < H; y++){
                row.push(
                    image.getPixelColor(x, y)
                );
            }
    data.push(row);
        }
    })
    .catch(err => console.error(err));
    
    return {data, W, H};
}

