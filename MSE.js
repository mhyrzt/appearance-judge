const loadImage = require("./loadImage");

exports.MSE = (imgCorrect, imgUser) => {
    imgCorrect  = loadImage.loadImage(imgCorrect);
    imgUser     = loadImage.loadImage(imgUser);
    const HC = imgCorrect.H;
    const WC = imgCorrect.W;
    const HU = imgUser.H;
    const WU = imgUser.W;

    if (!(HC == HU && WC == WU)) {
        console.error("IMAGES MUST BE SAME SIZE.");
        return;
    }

    const dataUser = imgUser.data;
    const dataCorrect = imgCorrect.data;

    let sum = 0;
    for (let x = 0; x < WC; x++) {
        for (let y = 0; y < HC; y++) {
            const u = dataUser[x][y];
            const c = dataCorrect[x][y];
            sum += (c - u) ** 2;
        }
    }

    return sum;
}
