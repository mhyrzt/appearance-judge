const loadImage = require("./loadImage");

exports.MAE = (imgCorrect, imgUser) => {
    imgCorrect  = loadImage.loadImage(imgCorrect);
    imgUser     = loadImage.loadImage(imgUser);
    const HC = imgCorrect.H;
    const WC = imgCorrect.W;
    const HU = imgUser.H;
    const WU = imgUser.W;

    if (!(HC == HU && WC == WU)) {
        console.error("IMAGES MUST BE SAME SIZE.");
        return;
    }

    const dataUser = imgUser.data;
    const dataCorrect = imgCorrect.data;

    let sum = 0;
    for (let x = 0; x < WC; x++) {
        for (let y = 0; y < HC; y++) {
            const u = dataUser[x][y];
            const c = dataCorrect[x][y];
            sum +=  Math.abs(c - u);
        }
    }

    return sum;
}
