const mse = require('./MSE');
const mae = require('./MAE');

const correct = "./QCV_Correct.png"
const usersub = "./QCV_User.png"

console.log(`MAE ERROR = ${mae.MAE(correct, usersub)}`);
console.log(`MSE ERROR = ${mse.MSE(correct, usersub)}`);